---
title: "About Helm Basics Training"
linkTitle: About
menu:
  main:
    weight: 3
---


{{< blocks/cover title="About Helm Basics Training" image_anchor="bottom" height="min" >}}

{{< /blocks/cover >}}

{{< blocks/section color="light" type="section">}}

This **Training** is based on Helm v2.16.5.


## Additional information

* [Official Helm Docs](https://v2.helm.sh/docs/)


## Kubecon Talks about Helm

* [Building Helm Charts From the Ground Up: An Introduction to Kubernetes [I] - Amy Chen, Heptio](https://www.youtube.com/watch?v=vQX5nokoqrQ)
* [An Introduction to Helm - Matt Farina, Samsung SDS & Josh Dolitsky, Blood Orange](https://www.youtube.com/watch?v=Zzwq9FmZdsU)
* [Managing Helm Deployments with Gitops at CERN - Ricardo Rocha, CERN](https://www.youtube.com/watch?v=g9FQxzK9E_M)
* [Helm 3 Deep Dive - Taylor Thomas, Microsoft Azure & Martin Hickey, IBM](https://www.youtube.com/watch?v=afCRt5Gd6Rk)
* [Helm Chart Patterns [I] - Vic Iglesias, Google](https://www.youtube.com/watch?v=WugC_mbbiWU)
* [Continuous Delivery for Kubernetes Apps with Helm and ChartMuseum](https://www.youtube.com/watch?v=u3VqswB-TJo)

{{< /blocks/section >}}

